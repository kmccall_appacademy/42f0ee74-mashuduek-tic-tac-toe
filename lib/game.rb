require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'
require 'byebug'
class Game

  attr_reader :board

  def initialize(player_one, player_two)
    @board = Board.new
    @players = [player_one, player_two]
  end

  def current_player
    @players.first
  end

  def switch_players!
    @players.rotate!
  end

  def play_turn
    player = self.current_player
    player.display(self.board)
    pos = player.get_move
    #THE BELOW IS FOR ACTUAL PLAY
    # until self.board.empty?(pos)
    #   pos = player.get_move
    # end

    self.board.place_mark(pos, player.mark)
    self.switch_players!
  end

  def play
    self.play_turn until !self.board.winner.nil?
  end
end

# if $PROGRAM_NAME == __FILE__
#   mashu = HumanPlayer.new("Mashu", :X)
#   tommy = HumanPlayer.new("Tommy", :O)
#   game = Game.new(mashu, tommy)
#   game.play
# end
