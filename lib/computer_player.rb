require 'byebug'
class ComputerPlayer

  attr_reader :mark, :board

  def initialize(name)
    @name = name
  end

  def display(board)
    @board = board
  end

  def mark=(mark) #THIS METHOD IS ONLY FOR PASSING SPECS
  end

  def get_move
    self.board.grid.each.with_index do |row, i|
      row.each_with_index do |pos, j|
        coord = [i, j]
        if self.board.empty?(coord)
          self.board.place_mark(coord, :O)
          if !self.board.winner.nil?
            return coord
          else
            self.board.place_mark(coord, nil)
          end
        end
      end
    end
    rands_row = rand(3)
    rands_line = rand(3)
    return [rands_row, rands_line]
  end
end
