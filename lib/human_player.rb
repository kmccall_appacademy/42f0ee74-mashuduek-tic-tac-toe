require 'byebug'
class HumanPlayer

  attr_reader :mark, :name

  def initialize(name) #, mark) #THE MARK IS FOR ACTUAL PLAY
    @mark = mark
    @name = name
  end

  def display(board)
    print board.grid
  end

  def get_move
    puts "where would you like to move? In this format: 'ROW, LINE' "
    input = gets.chomp
    coord = []
    input.split(",").map! { |i| coord << (i.to_i) }
    coord
  end

end
