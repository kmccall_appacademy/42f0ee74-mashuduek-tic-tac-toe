require 'byebug'
class Board

  attr_reader :grid

  def initialize(grid=[[nil, nil, nil], [nil, nil, nil] , [nil, nil, nil]])
    @grid = grid
  end

  def display
    @grid
  end

  def place_mark(pos, mark)
    grid[pos[0]][pos[1]] = mark
  end

  def empty?(pos)
    return true if grid[pos[0]][pos[1]].nil?
    false
  end

  def write(board)
  end

  def winner
    #ROWS
    grid.each do |row|
      return :X if row.all? {|p| p == :X}
      return :O if row.all? {|p| p == :X}
    end

    #DIAGONALS
    r1, r2, r3 = grid[0], grid[1], grid[2]
    return :X if r1[0] == :X && r2[1] == :X && r3[2] == :X
    return :O if r1[0] == :X && r2[1] == :X && r3[2] == :X
    return :X if r1[2] == :X && r2[1] == :X && r3[0] == :X
    return :O if r1[2] == :X && r2[1] == :X && r3[0] == :X

    #LINES
    trans = grid.transpose
    trans.each do |line|
      return :X if line.all? {|p| p == :X}
      return :O if line.all? {|p| p == :O}
    end
    nil
  end

  def over?
    return false if grid.flatten.all? { |pos| pos.nil? }
    return true if !self.winner.nil?
    return false if self.winner.nil? && grid.flatten.any? { |pos| pos.nil? }
    tied = false
    tied = true if self.winner.nil? && grid.flatten.none? { |p| p == nil}
    tied
  end

end
